package br.com.imagem.elevador.model.entity;

import org.junit.Assert;
import org.junit.Test;

import br.com.imagem.elevador.model.enums.DirecaoRota;

public class RotaTest 
{
	@Test(expected=NullPointerException.class)
	public void naoDeveSelecionarProximoAndarQuandoDirecaoNula()
	{
		Rota rota = new Rota();
		rota.getProximoAndar();
	}

	@Test
	public void deveSelecionarProximoAndarQuandoDirecaoDefinida()
	{
		Rota rota = new Rota();
		rota.addAndar(2,0);
		rota.addAndar(3,2);
		rota.getProximoAndar();
		
		Assert.assertEquals(rota.getDirecao(),DirecaoRota.SUBIR);
	}

}
