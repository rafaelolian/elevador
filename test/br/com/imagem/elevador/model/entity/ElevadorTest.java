package br.com.imagem.elevador.model.entity;

import org.junit.Assert;
import org.junit.Test;

import br.com.imagem.elevador.model.enums.StatusElevador;
import br.com.imagem.elevador.model.exception.PesoMaximoException;
import br.com.imagem.elevador.model.factory.ElevadorFactory;

public class ElevadorTest 
{
	private ElevadorFactory elevadorFactory = new ElevadorFactory(); 
	
	@Test
	public void deveComecarNoPrimeiroAndarComPortasAbertas()
	{
		Elevador elevador = elevadorFactory.getElevador();
		
		Assert.assertEquals(elevador.getAndarAtual(), 0);
		Assert.assertTrue(elevador.isPortaAberta());
	}
	
	@Test
	public void deveAtualizarStatusParaSubindo()
	{
		Elevador elevador = elevadorFactory.getElevador();
		elevador.embarcarPassageiro(80);
		elevador.selecionarAndar(3);
		elevador.fecharPorta(false);
		elevador.movimentar(false);
		
		Assert.assertEquals(elevador.getStatus(), StatusElevador.SUBINDO);
	}
	
	@Test
	public void deveAtualizarStatusParaDescendo()
	{
		Elevador elevador = elevadorFactory.getElevador();
		elevador.embarcarPassageiro(80);
		elevador.selecionarAndar(3);
		elevador.fecharPorta(true);
		elevador.selecionarAndar(1);
		elevador.fecharPorta(false);
		elevador.movimentar(false);
		
		Assert.assertEquals(elevador.getStatus(), StatusElevador.DESCENDO);
	}
	
	@Test
	public void naoDeveAlterarRoraNoMeioDoCaminho()
	{
		Elevador elevador = elevadorFactory.getElevador();
		elevador.embarcarPassageiro(80);
		elevador.selecionarAndar(6);
		elevador.selecionarAndar(5);
		elevador.selecionarAndar(7);
		elevador.fecharPorta(true);
		elevador.selecionarAndar(2);
		elevador.fecharPorta(true);
		
		Assert.assertEquals(elevador.getAndarAtual(), 6);
	}
	
	@Test
	public void deveFecharPortaQuandoHaRota()
	{
		Elevador elevador = elevadorFactory.getElevador();
		elevador.embarcarPassageiro(80);
		elevador.selecionarAndar(1);
		elevador.fecharPorta(false);
		
		Assert.assertFalse(elevador.isPortaAberta());
	}
	
	@Test(expected=IllegalStateException.class)
	public void naoDeveFecharPortaQuandoNaoHaRota()
	{
		Elevador elevador = elevadorFactory.getElevador();
		elevador.fecharPorta(false);
	}
	
	@Test
	public void rotaDeveConterApenasAndaresNaoVisitados()
	{
		Elevador elevador = elevadorFactory.getElevador();
		elevador.embarcarPassageiro(80);
		elevador.selecionarAndar(1);
		elevador.selecionarAndar(2);
		elevador.selecionarAndar(3);
		elevador.fecharPorta(true);
		elevador.fecharPorta(true);
		
		Assert.assertFalse(elevador.getRota().getAndares().contains(1));
		Assert.assertFalse(elevador.getRota().getAndares().contains(2));
	}
	
	@Test
	public void devePararQuandoPortaForAberta()
	{
		Elevador elevador = elevadorFactory.getElevador();	
		elevador.embarcarPassageiro(80);
		elevador.selecionarAndar(1);
		elevador.fecharPorta(false);
		elevador.movimentar(false);
		elevador.abrirPorta();
		
		Assert.assertEquals(elevador.getStatus(), StatusElevador.PARADO);
		Assert.assertTrue(elevador.isPortaAberta());
	}

	@Test
	public void naoDevePararQuandoPortarForFechada()
	{
		Elevador elevador = elevadorFactory.getElevador();		
		elevador.embarcarPassageiro(80);
		elevador.selecionarAndar(1);
		elevador.fecharPorta(false);
		elevador.movimentar(false);
		
		Assert.assertEquals(elevador.getStatus(), StatusElevador.SUBINDO);
		Assert.assertFalse(elevador.isPortaAberta());
	}

	@Test
	public void deveCriarRotaComCapacidadeMenorQueAMaxima()
	{
		Elevador elevador = elevadorFactory.getElevador();
		elevador.embarcarPassageiro(80*8);
		elevador.desembarcarPassageiro(90);
		elevador.selecionarAndar(1);
		
		Assert.assertTrue(elevador.getRota().existeRotaPendente());
	}

	@Test(expected=PesoMaximoException.class)
	public void naoDeveCriarRotaComCapacidadeMaiorQueAMaxima()
	{
		Elevador elevador = elevadorFactory.getElevador();
		elevador.embarcarPassageiro(80*8);
		elevador.selecionarAndar(1);
	}

	@Test(expected=NullPointerException.class)
	public void naoDeveCriarRotaComCapacidadeNula()
	{
		Elevador elevador = elevadorFactory.getElevador();
		elevador.selecionarAndar(1);
	}

	@Test
	public void naoDeveMovimentarQuandoPortaAberta()
	{
		Elevador elevador = elevadorFactory.getElevador();
		elevador.embarcarPassageiro(80);
		elevador.selecionarAndar(1);
		elevador.movimentar(false);
		
		Assert.assertEquals(elevador.getStatus(), StatusElevador.PARADO);
		Assert.assertEquals(elevador.getAndarAtual(), 0);
	}

	@Test
	public void deveMovimentarQuandoPortaFechada()
	{
		Elevador elevador = elevadorFactory.getElevador();
		elevador.embarcarPassageiro(80);
		elevador.selecionarAndar(1);
		elevador.fecharPorta(false);
		elevador.movimentar(false);
		
		Assert.assertEquals(elevador.getStatus(), StatusElevador.SUBINDO);
		Assert.assertEquals(elevador.getAndarAtual(), 1);
	}

	@Test
	public void deveCriarRotaAssimQueSelecionarOsAndares()
	{
		Elevador elevador = elevadorFactory.getElevador();
		elevador.embarcarPassageiro(80);
		elevador.selecionarAndar(1);
		elevador.selecionarAndar(2);
		
		Assert.assertTrue(elevador.getRota().existeRotaPendente());
	}

	@Test
	public void naoDeveCriarRotaEnquantoNaoSelecionarAndares()
	{
		Elevador elevador = elevadorFactory.getElevador();
		elevador.embarcarPassageiro(80);
		
		Assert.assertFalse(elevador.getRota().existeRotaPendente());
	}
	
	@Test
	public void naoDeveIncluirNaRotaOAndarAtual()
	{
		Elevador elevador = elevadorFactory.getElevador();
		elevador.embarcarPassageiro(80);
		elevador.selecionarAndar(2);
		elevador.selecionarAndar(3);
		elevador.fecharPorta(true);
		elevador.selecionarAndar(2);
		
		Assert.assertFalse(elevador.getRota().getAndares().contains(2));
	}

	@Test(expected=IllegalStateException.class)
	public void naoDeveSelecionarAndarComPortaFechada()
	{
		Elevador elevador = new ElevadorFactory().getElevador();
		elevador.embarcarPassageiro(80);
		elevador.fecharPorta(false);
		elevador.selecionarAndar(2);
	}
}
