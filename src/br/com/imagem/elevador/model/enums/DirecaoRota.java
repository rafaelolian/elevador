package br.com.imagem.elevador.model.enums;

public enum DirecaoRota 
{
	SUBIR,
	DESCER;
}
