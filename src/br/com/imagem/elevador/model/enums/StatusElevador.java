package br.com.imagem.elevador.model.enums;

public enum StatusElevador 
{
	PARADO,
	SUBINDO,
	DESCENDO,
}
