package br.com.imagem.elevador.model.entity;

import java.util.Set;
import java.util.TreeSet;

import br.com.imagem.elevador.model.enums.DirecaoRota;

public class Rota 
{
	private DirecaoRota direcao;
	
	private Set<Integer> rotaSubida = new TreeSet<Integer>();
	
	private TreeSet<Integer> rotaDescida = new TreeSet<Integer>();

	public Set<Integer> getAndares(){
		Set<Integer> result = new TreeSet<Integer>();
		result.addAll(rotaSubida);
		result.addAll(rotaDescida);
		return result;
	}
	
	public void addAndar( int novoAndar, int andarAtual )
	{
		if( novoAndar != andarAtual )
		{
			if( novoAndar > andarAtual ) {
				rotaSubida.add(novoAndar);
			}
			else {
				rotaDescida.add(novoAndar);
			}
			
			if( direcao == null ) {
				direcao = (novoAndar > andarAtual) ? DirecaoRota.SUBIR : DirecaoRota.DESCER;
			}
		}
	}
	
	public int getProximoAndar()
	{
		if( direcao == null ){
			throw new NullPointerException();
		}
		
		int proximoAndar;
		
		if( direcao == DirecaoRota.SUBIR )
		{
			proximoAndar = rotaSubida.iterator().next();
			
			rotaSubida.remove(proximoAndar);				
		}
		else {
			proximoAndar = rotaDescida.last();
			
			rotaDescida.remove(proximoAndar);				
		}
		
		if( rotaDescida.isEmpty() && rotaSubida.isEmpty() ){
			direcao = null;
		}
		else if( rotaSubida.isEmpty() ){
			direcao = DirecaoRota.DESCER;
		}
		else {
			direcao = DirecaoRota.SUBIR;
		}
		
		return proximoAndar;
	}
	
	public boolean existeRotaPendente(){
		return ! rotaSubida.isEmpty() || ! rotaDescida.isEmpty();
	}

	public DirecaoRota getDirecao() {
		return direcao;
	}
}
