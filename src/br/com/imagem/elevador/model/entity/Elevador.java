package br.com.imagem.elevador.model.entity;

import br.com.imagem.elevador.model.enums.StatusElevador;
import br.com.imagem.elevador.model.exception.PesoMaximoException;

public class Elevador 
{
	private int andarAtual = 0;
	
	private boolean portaAberta = true;
	
	private StatusElevador status = StatusElevador.PARADO;

	private float peso;

	private Rota rota = new Rota();
			
	public static final float PESO_MAXIMO = 560;// 7 pessoas X 80 kg = 560kg
		
	public void fecharPorta(boolean movimentarAposFechar)
	{
		if( ! rota.existeRotaPendente() ){
			throw new IllegalStateException();
		}
		
		portaAberta = false;
		
		if( movimentarAposFechar ){
			movimentar(true);
		}
	}
	
	public void movimentar( boolean abrirPortaAposMovimentar )
	{
		if( ! portaAberta )
		{
			int proximoAndar = rota.getProximoAndar();
			
			if( proximoAndar > andarAtual )
			{
				status = StatusElevador.SUBINDO;
			}
			else {
				status = StatusElevador.DESCENDO;
			}
			
			andarAtual = proximoAndar;
			
			if( abrirPortaAposMovimentar ){
				abrirPorta();
			}
		}
	}

	public void abrirPorta()
	{
		portaAberta = true;
		status = StatusElevador.PARADO;
	}

	public void embarcarPassageiro( float pesoPassageiro )
	{
		if( status != StatusElevador.PARADO || ! portaAberta ){
			throw new IllegalStateException();
		}
		
		peso += pesoPassageiro; 
	}

	public void desembarcarPassageiro( float pesoPassageiro )
	{
		if( status != StatusElevador.PARADO || ! portaAberta ){
			throw new IllegalStateException();
		}

		peso -= pesoPassageiro; 
	}
	
	public void selecionarAndar( int novoAndar )
	{
		if( peso > PESO_MAXIMO ) {
			throw new PesoMaximoException();
		}
		
		if( peso <= 0F ){
			throw new NullPointerException();
		}
		
		if( portaAberta ) {
			rota.addAndar(novoAndar, andarAtual);
		} 
		else {
			throw new IllegalStateException();
		}
	}
	
	public int getAndarAtual() {
		return andarAtual;
	}

	public StatusElevador getStatus() {
		return status;
	}

	public boolean isPortaAberta() {
		return portaAberta;
	}

	public Rota getRota() {
		return rota;
	}
}
