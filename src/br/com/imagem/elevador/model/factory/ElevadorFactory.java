package br.com.imagem.elevador.model.factory;

import br.com.imagem.elevador.model.entity.Elevador;

public class ElevadorFactory 
{
	public Elevador getElevador(){
		return new Elevador();
	}
}
